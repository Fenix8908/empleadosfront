import { EmpleadoService } from './../../servicios/empleado.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Empleado } from 'src/app/modelos/empleado';
import { ConfirmationService } from 'primeng/api';
import { TipoIdService } from 'src/app/servicios/tipo-id.service';
import { TipoId } from 'src/app/modelos/tipo-id';


@Component({
  selector: 'app-lista-empleado',
  templateUrl: './lista-empleado.component.html',
  styleUrls: ['./lista-empleado.component.css'],
  providers: [ConfirmationService]
})
export class ListaEmpleadoComponent implements OnInit {

  empleadoFilter = {
    nombre: '',
    segundoNombre: '',
    apellido: '',
    segundoApellido: '',
    pais: '',
    documento: '',
    correo: '',
    estado: '',
    fechaRegistro: ''
  }
  empleados: Empleado[] = [];
  tipoLista: TipoId[] = [];
  p: number = 1;
  constructor(
    private empleadoService: EmpleadoService,
    private confirmationService: ConfirmationService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.obtenerEmpleados()

  }

  obtenerEmpleados() {
    this.empleadoService.listaEmpleadosService().subscribe(
      respuesta => {
        this.empleados = respuesta;
      }
    )
  }
 

  eliminarEmpleado(id: number) {
    this.confirmationService.confirm({
      message: 'Estas seguro de eliminar este registro?',
      accept: () => {
        this.empleadoService.eliminarEmpleadoService(id).subscribe({
          next: () => {
            this.toastr.success('empleado eliminado', 'OK', {
              timeOut: 3000, positionClass: 'toast-top-center'
            });
            this.obtenerEmpleados();
          },
          error: (err) => {
            this.toastr.error(err.error.info, 'Fallo', {
              timeOut: 3000, positionClass: 'toast-top-center',
            });
          }
        });
      }

    })
  }

}
