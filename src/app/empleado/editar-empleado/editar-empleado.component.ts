import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Area } from 'src/app/modelos/area';
import { Empleado } from 'src/app/modelos/empleado';
import { TipoId } from 'src/app/modelos/tipo-id';
import { AreaService } from 'src/app/servicios/area.service';
import { EmpleadoService } from 'src/app/servicios/empleado.service';
import { TipoIdService } from 'src/app/servicios/tipo-id.service';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.css']
})
export class EditarEmpleadoComponent implements OnInit {

  empleado: Empleado = null;

  areaLista: Area[] = [];
  tipoLista: TipoId[] = []
  areaTipo = {
    areaId: null,
    tipoId: null
  }


  constructor(
    private empleadoService: EmpleadoService,
    private toastService: ToastrService,
    private areaService: AreaService,
    private tipoIdService: TipoIdService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) { }
  ngOnInit(): void {
    const id = this.activateRoute.snapshot.params.id;
    this.cargarEmpleadoPorid(id);

    this.cargarAreas();
    this.cargarTipos();
  }

  cargarEmpleadoPorid(id: number) {
    this.empleadoService.obtenerEmpleadoPorId(id).subscribe(
      respuesta => {
        this.empleado = respuesta;
      },
      err => {
        this.toastService.error(err.error.mensaje, 'Fallo', {
          timeOut: 3000, positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      })
  }
  editarEmpleado(): void {
    const id = this.activateRoute.snapshot.params.id;
    this.empleadoService.editarEmpleadoService(this.empleado, this.areaTipo.areaId, this.areaTipo.tipoId, id).subscribe(
      respuesta => {
        this.toastService.success("Empleado actualizado", "OK", {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(["/lista"])
      },
      err => {
        this.toastService.error(err.error.info, "Fallo", {
          timeOut: 3000, positionClass: 'toast-top-center'
        })
      }
    );
  }


  cargarAreas() {
    this.areaService.listaAreasService().subscribe(
      respuesta => {
        this.areaLista = respuesta;
      }
    )
  }

  cargarTipos() {
    this.tipoIdService.listaTiposService().subscribe(
      respuesta => {
        this.tipoLista = respuesta;
      }
    )
  }



}
