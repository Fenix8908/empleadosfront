import { Empleado } from './../modelos/empleado';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  empleadoURL = "http://localhost:8080/empleado/";

  constructor(private http:HttpClient) { }

  listaEmpleadosService():Observable<Empleado[]>{
    return this.http.get<Empleado[]>(this.empleadoURL +"listar");
  }
  crearEmpleadoService(empleado:Empleado,id_area:number,id_tipo:number):Observable<any>{
    return this.http.post<any>(this.empleadoURL + `guardar/area/${id_area}/tipo/${id_tipo}`,empleado);
  }

  obtenerEmpleadoPorId(id:number):Observable<Empleado>{
    return this.http.get<Empleado>(this.empleadoURL + `detalle/${id}`)
  }
  eliminarEmpleadoService(id:number):Observable<any>{
    return this.http.delete(this.empleadoURL+`eliminar/${id}`);
  }
  editarEmpleadoService(empleado:Empleado,id_area:number,id_tipo:number,id_emp:number):Observable<any>{
    return this.http.put<any>(this.empleadoURL+`editar/area/${id_area}/tipo/${id_tipo}/emp/${id_emp}`,empleado);
  }
}
