import { Area } from './../modelos/area';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  areaURL="http://localhost:8080/area/"

  constructor(private http:HttpClient) { }

  listaAreasService():Observable<Area[]>{
   return this.http.get<Area[]>(this.areaURL + "lista");
  }

}
