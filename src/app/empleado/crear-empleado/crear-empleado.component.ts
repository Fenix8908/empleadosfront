import { TipoIdService } from './../../servicios/tipo-id.service';
import { AreaService } from './../../servicios/area.service';
import { TipoId } from './../../modelos/tipo-id';
import { Area } from './../../modelos/area';
import { ToastrService } from 'ngx-toastr';
import { EmpleadoService } from './../../servicios/empleado.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Empleado } from 'src/app/modelos/empleado';

@Component({
  selector: 'app-crear-empleado',
  templateUrl: './crear-empleado.component.html',
  styleUrls: ['./crear-empleado.component.css']
})
export class CrearEmpleadoComponent implements OnInit {

  dominioColombia = "cidenet.com.co ";
  dominioUsa = "cidenet.com.us ";
  empleadoRequest = {
    nombre: '',
    segundoNombre: '',
    apellido: '',
    segundoApellido: '',
    pais: '',
    documento: '',
    correo: '',
    estado: 'activo',
    fechaRegistro: ''
  }


  areaLista: Area[] = [];
  tipoLista: TipoId[] = []
  areaId: number;
  tipoId: number;

  constructor(
    private empleadoService: EmpleadoService,
    private toastService: ToastrService,
    private areaService: AreaService,
    private tipoIdService: TipoIdService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.cargarAreas();
    this.cargarTipos();
  }

  generarCorreo():string{
    let correo = "";
    if(this.empleadoRequest.pais === "Colombia"){
      correo = this.empleadoRequest.nombre +'.'+this.empleadoRequest.apellido+'@'+this.dominioColombia;
    }else{
      correo = this.empleadoRequest.nombre +'.'+this.empleadoRequest.apellido+'@'+this.dominioUsa;
    }
    return correo.toLowerCase();
  }

  cargarAreas() {
    this.areaService.listaAreasService().subscribe(
      respuesta => {
        this.areaLista = respuesta;
      }
    )
  }


  cargarTipos() {
    this.tipoIdService.listaTiposService().subscribe(
      respuesta => {
        this.tipoLista = respuesta;
      }
    )
  }

  guardarEmpleado() {
    console.log(this.generarCorreo());
    
    const empleado = new Empleado(this.empleadoRequest.nombre.toUpperCase(), this.empleadoRequest.segundoNombre.toUpperCase(),
      this.empleadoRequest.apellido.toUpperCase(), this.empleadoRequest.segundoApellido.toUpperCase()
      , this.empleadoRequest.pais, this.empleadoRequest.documento,
      this.generarCorreo(), this.empleadoRequest.estado, this.empleadoRequest.fechaRegistro);
    this.empleadoService.crearEmpleadoService(empleado, this.areaId, this.tipoId).subscribe(
      respuesta => {
        this.toastService.success('Empleado creado!!', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(["/lista"]);
      },
      err => {
        this.toastService.error(err.error.info, 'Fallo', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(["/crear"]);
      }
    )


  }

}
