import { TipoId } from './../modelos/tipo-id';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TipoIdService {

  tipoURL="http://localhost:8080/tipo/"
  constructor(private http:HttpClient) { }

  listaTiposService():Observable<TipoId[]>{
    return this.http.get<TipoId[]>(this.tipoURL + "lista");
  }

}
