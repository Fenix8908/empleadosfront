import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[inputsValidations]',
  providers: [{ provide: NG_VALIDATORS, useExisting: InputsValidationsDirective, multi: true }]
})
export class InputsValidationsDirective implements Validator {


  constructor() { }
  validate(control: AbstractControl): ValidationErrors {
    const input = <string>control.value;
    const expresionInputs = /[A-Z]/;
  
    if (input) {
      return;
    }

    if (expresionInputs.test(input)) {
      return { 'inputsValidations': { 'message': 'solo letras mayusculas de la "A" a la "Z" sin acentos ni ñ*' } }
    }
    return null;
  }

}
